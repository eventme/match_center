<?php
/*
Plugin Name: Match Center
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: A brief description of the Plugin.
Version: 1.0
Author: air
Author URI: https://red-carlos.com/
License: A "Slug" license name e.g. GPL2
*/

//if ( ! function_exists( 'carbon_get_post_meta' ) ) {
//    function carbon_get_post_meta( $id, $name, $type = null ) {
//        return false;
//    }
//}
//
//if ( ! function_exists( 'carbon_get_the_post_meta' ) ) {
//    function carbon_get_the_post_meta( $name, $type = null ) {
//        return false;
//    }
//}
//
//if ( ! function_exists( 'carbon_get_theme_option' ) ) {
//    function carbon_get_theme_option( $name, $type = null ) {
//        return false;
//    }
//}
//
//if ( ! function_exists( 'carbon_get_term_meta' ) ) {
//    function carbon_get_term_meta( $id, $name, $type = null ) {
//        return false;
//    }
//}
//
//if ( ! function_exists( 'carbon_get_user_meta' ) ) {
//    function carbon_get_user_meta( $id, $name, $type = null ) {
//        return false;
//    }
//}
//
//if ( ! function_exists( 'carbon_get_comment_meta' ) ) {
//    function carbon_get_comment_meta( $id, $name, $type = null ) {
//        return false;
//    }
//}

use Carbon_Fields\Container;
use Carbon_Fields\Field;



class MatchCenter {
    protected static $single_instance = null;
    private $plugin_slug = 'mc_';

    public static function get_instance() {
        if ( null === self::$single_instance ) {
            self::$single_instance = new self();
        }

        return self::$single_instance;
    }

    protected function __construct()
    {
//      get path to plugin directory
        $this->path = plugin_dir_url(__FILE__);


        register_activation_hook( __FILE__, array($this, 'run_on_activate') );
        add_action ('hourly_update_matches', array( $this, 'mc_get_pos_actual_matches_data') );
        add_action ('daily_update_matches', array( $this, 'mc_get_matches_from_base') );

        add_action( 'init', array($this, $this->plugin_slug .'init') );

        add_action( 'carbon_fields_register_fields', array($this, $this->plugin_slug .'attach_theme_options') );
        add_action( 'carbon_fields_post_meta_container_saved', array($this, $this->plugin_slug .'update_position_taxonomies') );
        if(isset($_GET['post_type']) && ($_GET['post_type']  == 'mc_match_center')) {
            add_action('admin_enqueue_scripts', array($this, 'load_scripts'));
            wp_enqueue_style( 'jquery-ui', '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.min.css' );
            wp_enqueue_script( 'jquery-ui-datepicker' );
        }
        if( wp_doing_ajax() ) {
            add_action('wp_ajax_mc_get_matches', array($this, $this->plugin_slug . 'get_matches_from_base'));
            add_action('wp_ajax_nopriv_mc_get_matches', array($this, $this->plugin_slug . 'get_matches_from_base'));
        }

        add_action('restrict_manage_posts', array($this, $this->plugin_slug . 'date_range_filter'));
        add_action('restrict_manage_posts', array($this, $this->plugin_slug . 'filter_matches_by_taxonomies'));
        add_action('disable_months_dropdown', array($this, $this->plugin_slug . 'custom_disable_months_dropdown'), 10, 2);

        add_filter('manage_mc_match_center_posts_columns', array($this, $this->plugin_slug . 'match_center_add_columns'));
        add_action('manage_mc_match_center_posts_custom_column', array($this, $this->plugin_slug . 'match_center_add_data_to_columns'), 10, 2 );


        add_action( 'pre_get_posts', array( $this, 'filterquery' ) );

    }

    public function mc_init() {
        $this->mc_register_post_type();
        $this->mc_register_taxonomy();

    }

    public function load_scripts() {
        wp_enqueue_script('match_center_load_main', $this->path . 'assets/js/match_center.js', array(), '', true );
        wp_enqueue_style('match_plugin.css', $this->path . 'assets/css/match_plugin.css', false);
        wp_localize_script('match_center_load_main', 'match_center_load_ajax',
            array(
                'url' => admin_url('admin-ajax.php'),
                'nonce' => wp_create_nonce('match_center-load-nonce'),
            )
        );
    }


    function run_on_activate() {
        if( !wp_next_scheduled( 'hourly_update_matches' ) )
        {
            wp_schedule_event( time(), 'hourly', 'hourly_update_matches' );
        }

        if( !wp_next_scheduled( 'daily' ) )
        {
            wp_schedule_event( time(), 'daily', 'daily_update_matches' );
        }

    }

    public function mc_register_post_type() {
        register_post_type('mc_match_center',
            [
                'labels'      => [
                    'name'          => __('Матч Центер'),
                    'singular_name' => __('Матч Центер'),
                ],
                'public'      => true,
                'has_archive' => true,
                'supports'    => array( 'title' )
            ]
        );
    }

    public function mc_get_pos_actual_matches_data() {
        global $wpdb;

        $args = array(
            'post_type' => 'mc_match_center',
            'posts_per_page' => -1,
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'mc_view_pos',
                    'field'    => 'slug',
                    'terms'    => array( 'mc_main_news', 'matches_schedule', 'matches_result' ),
                )
            ),
        );

        $posts = get_posts( $args );

        foreach ( $posts as $post ) {
            $cb_id = carbon_get_post_meta( $post->ID, 'crb_id' );

            $sql = $wpdb->prepare( 'SELECT cm.id AS id,
                   cm.result_1 AS result_1,
                   cm.status AS status,
                   cm.result_2 AS result_2,
                   cm.play_date AS play_date,
                   cm.play_time AS play_time

                  FROM foot_football_club_matches cm
                  WHERE cm.id = %d', $cb_id );

            $matches = $wpdb->get_results( $sql );

            carbon_set_post_meta( $post->ID, 'crb_result_1', $matches[0]->result_1 );
            carbon_set_post_meta( $post->ID, 'crb_result_2', $matches[0]->result_2 );
            carbon_set_post_meta( $post->ID, 'crb_play_date', $matches[0]->play_date );
            carbon_set_post_meta( $post->ID, 'crb_play_time', $matches[0]->play_time );

        }

    }

    public function mc_get_matches_from_base() {
        check_ajax_referer( 'match_center-load-nonce', 'nonce_code' );
//        get all current matches by range date

        $args = array(
            'post_type' => 'mc_match_center',
            'posts_per_page' => -1,
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => '_crb_play_date',
                    'value' => date('Y-m-d', strtotime("-1 week")),
                    'compare' => '>',
                    'type' => 'DATE'
                ),
                array(
                    'key' => '_crb_play_date',
                    'value' => date('Y-m-d', strtotime("+2 week")),
                    'compare' => '<',
                    'type' => 'DATE'
                ),

            )
        );
        $post_ids = [];
        $post_id = '';
        $posts = get_posts( $args );
//        get all matches id from custom table with post id value
        foreach ( $posts as $post ) {
            $cb_id = carbon_get_post_meta( $post->ID, 'crb_id' );
            $post_ids[$cb_id] = $post->ID;
        }

//        SELECT ctt.name AS name_parent, ct.name AS name_child, cm.result_1 AS result_1, cm.result_2 AS result_2, cm.play_date AS play_date, cm.play_time AS play_time, pc1.name AS club_name_1, pc2.name AS club_name_2, cm.id AS id FROM foot_football_competition_term_relationships ctr LEFT OUTER JOIN foot_football_competition_term ctt ON ctt.id = ctr.competition_term_id LEFT OUTER JOIN parser_common_tournament ct ON ct.id = ctr.competition_id LEFT OUTER JOIN foot_football_club_matches cm ON cm.competition_id = ct.id LEFT OUTER JOIN parser_common_club pc1 ON cm.club_id_1 = pc1.id LEFT OUTER JOIN parser_common_club pc2 ON cm.club_id_2 = pc2.id WHERE cm.status = 0


        global $wpdb;
        $sql = "SELECT cm.id AS id,
                   cm.result_1 AS result_1,
                   fcc.name AS competition_name,
                   cm.season_id AS season,
                   cm.status AS status,
                   cm.result_2 AS result_2,
                   cm.play_date AS play_date,
                   cm.play_time AS play_time,
                   pc1.name AS club_name_1,
                   pc2.name AS club_name_2
                  FROM foot_football_club_matches cm
                  LEFT OUTER JOIN parser_common_club pc1 ON cm.club_id_1 = pc1.id
                  LEFT OUTER JOIN parser_common_club pc2 ON cm.club_id_2 = pc2.id
                  LEFT OUTER JOIN parser_common_tournament fcc ON fcc.id = cm.competition_id 
                  WHERE play_date between '" . date('Y-m-d', strtotime("-1 week")) . "' and '" . date('Y-m-d', strtotime("+2 week")) . "'";
        $matches = $wpdb->get_results( $sql );
        $i = 0;
        foreach ( $matches as $match ) {
//            if ( $i < 10  ) {
//                $i++;
//                    if current match exist - update else - add
                if ( isset($post_ids[$match->id]) ) {
                    $post = get_post($post_ids[$match->id]);

                    $post_id = $post->ID;

                     carbon_set_post_meta($post_id, 'crb_result_1', $match->result_1);
                     carbon_set_post_meta($post_id, 'crb_result_2', $match->result_2);
                     carbon_set_post_meta($post_id, 'crb_status', $match->status);
                     carbon_set_post_meta($post_id, 'crb_play_date', $match->play_date);
                     carbon_set_post_meta($post_id, 'crb_play_time', $match->play_time);
                } else {
                    $post_id = wp_insert_post(array (
                        'post_type' => 'mc_match_center',
//                        'post_title' => $match->club_name_1 . ' - ' . $match->club_name_2,
                        'post_title' => $match->id,
                        'post_status' => 'publish',
                    ));

                    $post_ids[$match->id] = $post_id;

                    if ($post_id) {
                        carbon_set_post_meta($post_id, 'crb_id', $match->id);
                        carbon_set_post_meta($post_id, 'crb_team_1', $match->club_name_1);
                        carbon_set_post_meta($post_id, 'crb_team_2', $match->club_name_2);
                        carbon_set_post_meta($post_id, 'crb_result_1', $match->result_1);
                        carbon_set_post_meta($post_id, 'crb_result_2', $match->result_2);
                        carbon_set_post_meta($post_id, 'crb_play_date', $match->play_date);
                        carbon_set_post_meta($post_id, 'crb_play_time', $match->play_time);
                        carbon_set_post_meta($post_id, 'crb_status', $match->status);
                        carbon_set_post_meta($post_id, 'crb_season', $match->season);
                        carbon_set_post_meta($post_id, 'crb_competition', $match->competition_name);
                    }
                } //end else



//              add categories and if exist add to post
                $sql = "SELECT ctt.name AS name_parent, ct.name AS name_child  FROM foot_football_competition_term_relationships ctr 
                LEFT OUTER JOIN foot_football_competition_term ctt ON ctt.id = ctr.competition_term_id 
                LEFT OUTER JOIN parser_common_tournament ct ON ct.id = ctr.competition_id 
                LEFT OUTER JOIN foot_football_club_matches cm ON cm.competition_id = ct.id WHERE cm.id =" . $match->id;

                $categories = $wpdb->get_results( $sql );



                $set_the_terms = [];
                foreach ( $categories as $category ) {
                    $children_term_slug = $category->name_child .'-' . $category->name_parent;


                    if ( !term_exists( $category->name_parent, 'mc_tournament' ) ) {
                        wp_insert_term(
                            $category->name_parent, // the term
                            'mc_tournament', // the taxonomy
                            array(
                                'slug' => $category->name_parent,
                            )
                        );
                    }


                    if ( !term_exists( $children_term_slug, 'mc_tournament' ) ) {
                        $term = get_term_by('slug', $category->name_parent, 'mc_tournament');

                        wp_insert_term(
                            $category->name_child,
                            'mc_tournament',
                            array(
                                'slug' => $children_term_slug,
                                'parent'  => $term->term_id,
                            )
                        );

                    }
                    array_push( $set_the_terms, $children_term_slug );
                    array_push( $set_the_terms, $category->name_parent );


                }
                wp_set_object_terms( $post_id, $set_the_terms, 'mc_tournament' );
                delete_option("mc_tournament_children");
                wp_cache_flush();

//            }

        }

        wp_die();
    }

    public function mc_register_taxonomy() {
        $labels = array(
            'name'              => _x( 'Позиция', 'taxonomy general name', 'textdomain' ),
            'singular_name'     => _x( 'ПозицияПозиция', 'taxonomy singular name', 'textdomain' ),
            'search_items'      => __( 'Искать позиции', 'textdomain' ),
            'all_items'         => __( 'Все позиции', 'textdomain' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
        );

        register_taxonomy( 'mc_view_pos', array( 'mc_match_center' ), $args );

        $labels = array(
            'name'              => _x( 'Турниры', 'taxonomy general name', 'textdomain' ),
            'singular_name'     => _x( 'Турнир', 'taxonomy singular name', 'textdomain' ),
            'search_items'      => __( 'Искать позиции', 'textdomain' ),
            'all_items'         => __( 'Все позиции', 'textdomain' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
        );

        register_taxonomy( 'mc_tournament', array( 'mc_match_center' ), $args );
    }





    public function mc_attach_theme_options() {
        Container::make( 'post_meta', __( 'Custom Data', 'crb' ) )
            ->where( 'post_type', '=', 'mc_match_center' )
            ->add_fields( [
                Field::make( 'text', 'crb_id', 'id' )->set_width(33),
                Field::make( 'date', 'crb_play_date', 'Дата игры' )->set_width(33),
                Field::make( 'time', 'crb_play_time', 'Время игры' )
                    ->set_picker_options( array('time_24hr' => true) )
                    ->set_input_format( 'H:i', 'H:i' )
                    ->set_storage_format( 'H:i' )
                    ->set_width(33),
//
                Field::make( 'separator', 'crb_separator_1', '' ),

                Field::make( 'text', 'crb_competition', 'Турнир' )->set_width(33),
                Field::make( 'text', 'crb_status', 'Статус' )->set_width(33),
                Field::make( 'text', 'crb_season', 'Сезон' )->set_width(33),
                Field::make( 'separator', 'crb_separator_2', '' ),
                Field::make( 'text', 'crb_team_1', 'Команда 1' )->set_width(40),
                Field::make( 'text', 'crb_result_1', 'Счет' )->set_width(10),
                Field::make( 'text', 'crb_result_2', 'Счет' )->set_width(10),
                Field::make( 'text', 'crb_team_2', 'Команда 2' )->set_width(40),

                Field::make( 'complex', 'crb_broadcast_data', 'Данные трянсляции' )
                    ->set_layout( 'tabbed-horizontal' )
                    ->add_fields( [
                        Field::make( 'date_time', 'crb_start', 'Начало трансляции' )->set_width(25),
                        Field::make( 'date_time', 'crb_end', 'Конец трансляции' )->set_width(25),
                        Field::make( 'select', 'crb_position_tax_id', 'Позиция' )->set_width(25)
                            ->add_options( [$this, $this->plugin_slug .'get_tax_position'])
                    ]),



            ]);

    }

//  add position taxonomy for repeater select
    public function mc_get_tax_position() {
        $defaults = array(
            'taxonomy' => 'mc_view_pos',
            'hide_empty' => false,
        );

        $terms = get_terms( $defaults );
        $terms_array = array();
        if ( ! empty( $terms ) ) {
            foreach ( $terms as $term ) {
                $terms_array[$term->term_id] = $term->name;
            }
        }
        return $terms_array;
    }

//  update position taxonomy according to repeater information
    public function mc_update_position_taxonomies( $post_id ) {
        $repeater = carbon_get_post_meta( $post_id, 'crb_broadcast_data' );
        $set_the_terms = array();

        foreach( $repeater as $item ) {
            $term = get_term_by( 'id' , $item['crb_position_tax_id'], 'mc_view_pos' );
            array_push( $set_the_terms, $term->slug );
        }
        wp_set_object_terms( $post_id, $set_the_terms , 'mc_view_pos' );
    }

//      get matches by category
//      get all for optimization Don't sent request for every shortcode
//      send one request create one array and use it
    public function mc_get_matches_array() {
        $args = array(
            'post_type' => 'mc_match_center',
            'posts_per_page' => -1,
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => '_crb_play_date',
                    'value' => date('Y-m-d', strtotime("-1 week")),
                    'compare' => '>',
                    'type' => 'DATE'
                ),
                array(
                    'key' => '_crb_play_date',
                    'value' => date('Y-m-d', strtotime("+2 week")),
                    'compare' => '<',
                    'type' => 'DATE'
                ),

            ),
            'tax_query' => array(
                array(
                    'taxonomy' => 'mc_view_pos',
                    'field'    => 'slug',
                    'terms'    => array('mc_main_news', 'matches_schedule', 'matches_result'),
                ),
            ),
        );
        $posts = get_posts( $args );
        $posts_in_date = [];


//        filter matches according to date
        foreach ( $posts as $post ) {
            $items = carbon_get_post_meta( $post->ID, 'crb_broadcast_data' );

            foreach ( $items as $item ) {
                $start_date = $item['crb_start'];
                $end_date = $item['crb_end'];
                $now = $this->mc_get_datetime_now();

                if ( $this->check_in_range($start_date, $end_date, $now ) ) {
                    $term = get_term_by( 'id' , $item['crb_position_tax_id'], 'mc_view_pos' );
                    $posts_in_date[$term->slug][] = $post;
                }
            }
        }


        return $posts_in_date;
    }
//    check that date in given range
    public function check_in_range($start_date, $end_date, $date_from_user) {
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $user_ts = strtotime($date_from_user);

        return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
    }

//    get time in particular time zone
    public function mc_get_datetime_now() {
        $tz_object = new DateTimeZone('Europe/Kiev');
        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);

        return $datetime->format('Y\-m\-d\ h:i:s');
    }


    public function sort_by_date( $a, $b ) {
        $play_date_a = carbon_get_post_meta( $a->ID, 'crb_play_date' );
        $play_date_b = carbon_get_post_meta( $b->ID, 'crb_play_date' );

        return strtotime($play_date_a) < strtotime($play_date_b) ? -1 : 1;
    }

    public function mc_top_shortcode() {
        $instance = MatchCenter::get_instance();
//        $posts = $instance->mc_get_matches_array();
//        $innerHTML = "<div class='top_matches_list'>";
//
//        usort($posts['mc_main_news'], array($instance, "sort_by_date"));
//
//        foreach ( $posts['mc_main_news'] as $post ) {
//            $team_1 = carbon_get_post_meta( $post->ID, 'crb_team_1' );
//            $team_2 = carbon_get_post_meta( $post->ID, 'crb_team_2' );
//            $play_date = carbon_get_post_meta( $post->ID, 'crb_play_date' );
//            $play_time = carbon_get_post_meta( $post->ID, 'crb_play_time' );
//            $result_1 = carbon_get_post_meta( $post->ID, 'crb_result_1' );
//            $result_2 = carbon_get_post_meta( $post->ID, 'crb_result_2' );
//            $date = DateTime::createFromFormat('Y-m-d', $play_date);
//            $terms = wp_get_post_terms( $post->ID, 'mc_tournament' );
//            $term_name = '';
//            if ( $terms[0] ) {
//                $term_name = $terms[0]->name;
//            }
//
//            $innerHTML .= '<div class="item_top_match" style=" position: relative;">
//                                <span>' . $term_name . '</span>
//                                <div class="box_top_match">
//                                    <div class="teams"><span>'. $team_1 .'</span><span>'. $team_2 .'</span></div>
//                                    <div class="top_block_score" style="
//    position: absolute;
//    left: 50%;
//    transform: translateX(-50%);
//"><span>'. $result_1 .'</span><span>'. $result_2 .'</span></div>
//                                    <div class="data_play_match"><span>' . $date->format('d.m') . '.</span><span>' . $play_time . ' мск
//</span></div>
//                                </div>
//                         </div>';
//        }
//        $innerHTML .= "</div>";
//
//        return $innerHTML;
    }


    public function mc_schedule_shortcode() {
        $instance = MatchCenter::get_instance();
        $posts = $instance->mc_get_matches_array();
        $innerHTML = "<div class='result_matches_home'>";
        $innerHTML .= "<div class='block-title'><span>Расписание  матчей </span></div>";

        usort($posts['matches_schedule'], array($instance, "sort_by_date"));

        foreach ( $posts['matches_schedule'] as $post ) {
            $team_1 = carbon_get_post_meta( $post->ID, 'crb_team_1' );
            $team_2 = carbon_get_post_meta( $post->ID, 'crb_team_2' );
            $play_date = carbon_get_post_meta( $post->ID, 'crb_play_date' );
            $date = DateTime::createFromFormat('Y-m-d', $play_date);


            $innerHTML .= "<div class='item_result_match item_result_match_teams item_result_match_2'>
    								<span class='play_date'>".date_format($date, 'd.m.')."</span>
    								<span class='team_1'>". $team_1 ."</span> <span class='result result_1'>-:-</span> <span class='team_2'>".$team_2."</span>
    							</div>";


        }
        $innerHTML .= "</div>";

        return $innerHTML;
    }

    public function mc_result_shortcode() {
        $instance = MatchCenter::get_instance();
        $posts = $instance->mc_get_matches_array();
        $innerHTML = "<div class='result_matches_home'>";
        $innerHTML .= "<div class='block-title'><span>Результаты матчей </span></div>";
        usort($posts['matches_result'], array($instance, "sort_by_date"));

        foreach ( $posts['matches_result'] as $post ) {
            $team_1 = carbon_get_post_meta( $post->ID, 'crb_team_1' );
            $team_2 = carbon_get_post_meta( $post->ID, 'crb_team_2' );
            $result_1 = carbon_get_post_meta( $post->ID, 'crb_result_1' );
            $result_2 = carbon_get_post_meta( $post->ID, 'crb_result_2' );
            $play_date = carbon_get_post_meta( $post->ID, 'crb_play_date' );
            $date = DateTime::createFromFormat('Y-m-d', $play_date);

            $teams = '<span class="team_1">'.$team_1.'</span> <span class="result result_1">'.$result_1.':'.$result_2.'</span> <span class="team_2">'.$team_2.'</span>';

            if ( $result_1 > $result_2 ) {
                $teams = '<span class="team_1"><b>'.$team_1.'</b></span><span class="result result_1">'.$result_1.':'.$result_2.'</span> <span class="team_2">'.$team_2.'</span>';
            }

            if ( $result_2 > $result_1 ) {
                $teams = '<span class="team_1">'.$team_1.'</span> <span class="result result_1">'.$result_1.':'.$result_2.'</span> <span class="team_2"><b>'.$team_2.'</b></span>';
            }


            $innerHTML .= "<div class='item_result_match item_result_match_teams item_result_match_2'>
    								<span class='play_date'>".date_format($date, 'd.m.')."</span>
    								".$teams."
    							</div>";

        }
        $innerHTML .= "</div>";

        return $innerHTML;
    }



    public function mc_date_range_filter(){

        $from = ( isset( $_GET['matches_date_from'] ) && $_GET['matches_date_from'] ) ? $_GET['matches_date_from'] : '';
        $to = ( isset( $_GET['matches_date_to'] ) && $_GET['matches_date_to'] ) ? $_GET['matches_date_to'] : '';

        echo '<style>
		input[name="matches_date_from"], input[name="matches_date_to"]{
			line-height: 28px;
			height: 28px;
			margin: 0;
			width:125px;
		}
		</style>
 
		<input type="text" name="matches_date_from" placeholder="Начало" value="' . $from . '" />
		<input type="text" name="matches_date_to" placeholder="Окончание" value="' . $to . '" />
 
';

    }

    public function filterquery( $admin_query ){
        global $pagenow;

//        if (
//            is_admin()
//            && $admin_query->is_main_query()
//            && in_array( $pagenow, array( 'edit.php', 'upload.php' ) )
//            && ( ! empty( $_GET['matches_date_from'] ) || ! empty( $_GET['matches_date_to'] ) )
//        ) {


        $post_types = get_post_types(array('_builtin' => false), 'names');
        $post_type = $admin_query->get('post_type');
        if(in_array($post_type, $post_types) && $post_type == 'mc_match_center' ) {
            $query_array = ['relation' => 'AND'];

            if ( ! empty( $_GET['matches_date_from'] ) ) {
                $query_array[] = array(
                    'key' => '_crb_play_date',
                    'value' => $_GET['matches_date_from'],
                    'compare' => '>',
                    'type' => 'DATE'
                );
            }

            if ( ! empty( $_GET['matches_date_from'] ) ) {
                $query_array[] = array(
                    'key' => '_crb_play_date',
                    'value' => $_GET['matches_date_to'],
                    'compare' => '<',
                    'type' => 'DATE'
                );
            }

            if ( ! empty( $_GET['matches_date_from'] ) || ! empty( $_GET['matches_date_to'] ) ) {
                $admin_query->set(
                    'meta_query',
                    $query_array
                );
            }

        }

//        }
    }

    public function mc_filter_matches_by_taxonomies( $post_type, $which ) {

        // Apply this only on a specific post type
        if ( 'mc_match_center' !== $post_type )
            return;

        $taxonomies = array( 'mc_tournament' );

        foreach ( $taxonomies as $taxonomy_slug ) {

            // Retrieve taxonomy data
            $taxonomy_obj = get_taxonomy( $taxonomy_slug );
            $taxonomy_name = $taxonomy_obj->labels->name;

            // Retrieve taxonomy parent terms
            $parent_terms = get_terms(
                array( 'taxonomy' => $taxonomy_slug, 'parent' => 0,  'hide_empty' => true, )
            );

            // Display filter HTML
            foreach ($parent_terms as $parent_term) {
                $terms = get_term_children( $parent_term->term_id, 'mc_tournament' );
                echo "<select name='{$taxonomy_slug}' id='{$taxonomy_slug}_" . $parent_term->term_id . "' class='postform'>";
                echo '<option value="">' . sprintf( esc_html__( '%s', 'text_domain' ), $parent_term->name ) . '</option>';
                foreach ( $terms as $term_id ) {
                    $term = get_term_by( 'id', $term_id, 'mc_tournament' );
                    if ( $term->count > 0) {
                        printf(
                            '<option value="%1$s" %2$s>%3$s (%4$s)</option>',
                            $term->slug,
                            ( ( isset( $_GET[$taxonomy_slug] ) && ( $_GET[$taxonomy_slug] == $term->slug ) ) ? ' selected="selected"' : '' ),
                            $term->name,
                            $term->count
                        );
                    }
                }
                echo '</select>';
            }

        }

        $taxonomies = array( 'mc_view_pos' );
        foreach ( $taxonomies as $taxonomy_slug ) {

            // Retrieve taxonomy data
            $taxonomy_obj = get_taxonomy( $taxonomy_slug );
            $taxonomy_name = $taxonomy_obj->labels->name;

            // Retrieve taxonomy terms
            $terms = get_terms( $taxonomy_slug );

            // Display filter HTML
            echo "<select name='{$taxonomy_slug}' id='{$taxonomy_slug}' class='postform'>";
            echo '<option value="">' . sprintf( esc_html__( '%s', 'text_domain' ), $taxonomy_name ) . '</option>';
            foreach ( $terms as $term ) {
                printf(
                    '<option value="%1$s" %2$s>%3$s (%4$s)</option>',
                    $term->slug,
                    ( ( isset( $_GET[$taxonomy_slug] ) && ( $_GET[$taxonomy_slug] == $term->slug ) ) ? ' selected="selected"' : '' ),
                    $term->name,
                    $term->count
                );
            }
            echo '</select>';
        }



    }

    public function mc_custom_disable_months_dropdown( $false , $post_type ) {
        $disable_months_dropdown = $false;
        $disable_post_types = array( 'mc_match_center' );
        if( in_array( $post_type , $disable_post_types ) ) {
            $disable_months_dropdown = true;
        }

        return $disable_months_dropdown;

    }






public function mc_match_center_add_columns( $columns ) {
        $columns['match_name'] = 'Имя команд';
        $columns['match_competition'] = 'Турнир';
        $columns['match_date'] = 'Дата игры';
        $columns['match_time'] = 'Время игры';
        $columns['match_translation'] = 'Позиция';
        $columns['match_status'] = 'Статус';
        $columns['match_season'] = 'Сезон';

        return $columns;
    }

    public function mc_match_center_add_data_to_columns( $column, $id ) {
            if( $column == 'match_name' ) {
                $first_name = carbon_get_post_meta( $id, 'crb_team_1' );
                $sec_name = carbon_get_post_meta( $id, 'crb_team_2' );

                echo $first_name . ' - ' . $sec_name;
            }
            if( $column == 'match_date' ) {
                echo carbon_get_post_meta( $id, 'crb_play_date' );
            }
            if( $column == 'match_time' ) {
                echo carbon_get_post_meta( $id, 'crb_play_time' );
            }
            if( $column == 'match_status' ) {
                echo carbon_get_post_meta( $id, 'crb_status' );
            }
            if( $column == 'match_season' ) {
                echo carbon_get_post_meta( $id, 'crb_season' );
            }
            if( $column == 'match_competition' ) {
                echo carbon_get_post_meta( $id, 'crb_competition' );
            }
            if( $column == 'match_translation' ) {
                // echo carbon_get_post_meta( $id, 'crb_play_time' );
                $items = carbon_get_post_meta( $id, 'crb_broadcast_data' );
                foreach ( $items as $item ) {
                    $start_date = $item['crb_start'];
                    $end_date = $item['crb_end'];

                    $term = get_term_by( 'id' , $item['crb_position_tax_id'], 'mc_view_pos' );

                    echo $term->name . '<br>' . $start_date  . '<br>' . $end_date . '<br>';

                }
            }

        }

}

MatchCenter::get_instance();

add_shortcode( 'top_matches_shortcode', array( 'MatchCenter', 'mc_top_shortcode' ) );
//add_shortcode( 'home_schedule_shortcode', array( 'MatchCenter', 'mc_schedule_shortcode' ) );
////schedule_of_home
//add_shortcode( 'home_result_shortcode', array( 'MatchCenter', 'mc_result_shortcode' ) );
//



