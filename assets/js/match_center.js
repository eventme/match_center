jQuery( document ).ready(function( $ ) {
    jQuery(function(){
        jQuery("body.post-type-mc_match_center .wrap h1").append('<button class="parse_date">Обновить данные</button>');

        $(document).on('click', '.parse_date', function() {
            $('.parse_date').prop('disabled', true);
            $.ajax({
                type : 'POST',
                url  : match_center_load_ajax.url,
                data : {
                    action : 'mc_get_matches',
                    nonce_code : match_center_load_ajax.nonce,
                },
            }).done(function (data) {
                location.reload();
            });
        });
    });


    var from = $('input[name="matches_date_from"]'),
    to = $('input[name="matches_date_to"]');

    $('input[name="matches_date_from"], input[name="matches_date_to"]' ).datepicker({ dateFormat: 'yy-mm-dd' });
    // $( 'input[name="matches_date_from"], input[name="matches_date_to"]' ).datepicker( "option", "dateFormat", "yy-mm-dd" );
    // by default, the dates look like this "April 3, 2017" but you can use any strtotime()-acceptable date format
    // to make it 2017-04-03, add this - datepicker({dateFormat : "yy-mm-dd"});


    // the rest part of the script prevents from choosing incorrect date interval
//     from.on( \'change\', function() {
//     to.datepicker( \'option\', \'minDate\', from.val() );
// });
//
// to.on( \'change\', function() {
// from.datepicker( \'option\', \'maxDate\', to.val() );
// });
});